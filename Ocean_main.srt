﻿1
00:00:00,000 --> 00:00:06,000
Smelly cat, smelly cat, what are they feeding you?

2
00:00:06,200 --> 00:00:14,590
Smelly cat, smelly cat, it's not your fault...

3
00:00:15,000 --> 00:00:15,590
that's crazy.

4
00:00:15,600 --> 00:00:18,590
You could totally sell.

5
00:00:18,600 --> 00:00:22,390
This would be perfect for like a kitty litter campaign,

6
00:00:22,400 --> 00:00:25,390
a jingle.

7
00:00:25,590 --> 00:00:26,500
No, no, no

8
00:00:26,700 --> 00:00:28,690
Why not? You would make a ton of money.

9
00:00:28,700 --> 00:00:33,000
OK, if I was in this for the money, I would be a millionaire by now.

10
00:00:33,800 --> 00:00:35,090
How to get out of that jingle head.

11
00:00:35,400 --> 00:00:37,990
You're right,

12
00:00:38,000 --> 00:00:38,490
you're right?

13
00:00:38,500 --> 00:00:39,190
I'm sorry.

14
00:00:39,200 --> 00:00:42,390
I'm going to play a song now.

15
00:00:42,400 --> 00:00:43,390
That's really,

16
00:00:43,400 --> 00:00:43,990
really sad.

17
00:00:44,000 --> 00:00:46,490
Fox make stuff.

18
00:00:52,600 --> 00:00:54,290
I thought you were coming.

19
00:00:58,000 --> 00:00:58,690
Okay,

20
00:00:58,700 --> 00:01:00,090
don't get mad.

21
00:01:00,300 --> 00:01:01,590
give me a reason.

22
00:01:04,000 --> 00:01:06,290
I played smelly cat for the people of my old

23
00:01:06,300 --> 00:01:06,690
Ad Agency.

24
00:01:06,700 --> 00:01:07,690
They went nuts.

25
00:01:07,700 --> 00:01:11,190
I told you that I didn't want you to try

26
00:01:11,200 --> 00:01:14,390
and sell it and you just a big fat did

27
00:01:14,400 --> 00:01:14,890
it anyway.

28
00:01:14,900 --> 00:01:18,190
You know what,

29
00:01:18,400 --> 00:01:20,790
five years ago I probably would have done anything to

30
00:01:20,800 --> 00:01:23,290
play with you but I can't do it by myself

31
00:01:23,300 --> 00:01:25,090
writing and if I can't trust you,

32
00:01:25,100 --> 00:01:25,990
then just forget it.

33
00:01:26,000 --> 00:01:27,690
I don't want to forget it.

34
00:01:27,700 --> 00:01:28,290
Okay.

35
00:01:28,600 --> 00:01:29,490
You have to choose.

36
00:01:29,500 --> 00:01:30,090
All right.

37
00:01:30,100 --> 00:01:33,490
If the most important thing on the planet to you,

38
00:01:33,500 --> 00:01:35,490
is he can't prove anything then,

39
00:01:35,500 --> 00:01:37,290
okay,

40
00:01:37,300 --> 00:01:38,490
you can have smelly cat,

41
00:01:38,500 --> 00:01:39,990
but we won't be partners.

42
00:01:41,300 --> 00:01:45,790
So it's going to be a problem order in litter

43
00:01:45,800 --> 00:01:46,290
box,

44
00:01:46,300 --> 00:01:49,190
tell change your kitty change your kitty litter.

45
00:01:55,300 --> 00:01:55,890
Sorry.

46
00:01:58,100 --> 00:01:58,490
UK.

47
00:02:00,000 --> 00:02:00,390
Yeah,

48
00:02:00,400 --> 00:02:01,590
I actually am.

49
00:02:01,600 --> 00:02:04,790
Yeah that's going to hand you all kinds of stuff.

50
00:02:04,800 --> 00:02:06,490
You know,

51
00:02:06,500 --> 00:02:08,690
you learn your little lessons and hopefully grow.

52
00:02:09,600 --> 00:02:11,490
Want to hear new song.

53
00:02:19,800 --> 00:02:20,790
Go to hell.

54
00:02:20,800 --> 00:02:22,190
Jingle hard soda.